from datetime import datetime, timedelta
from dateutil.rrule import rrule, DAILY, MO, TU, WE, TH, FR

# List of holidays (example format: 'YYYY-MM-DD')
holidays = ['2024-12-25', '2024-01-01']  # Update this list as needed

def parse_date(input_date):
    """Parse the user input date string and return a datetime object."""
    try:
        return datetime.strptime(input_date, '%Y-%m-%d %H:%M:%S')
    except ValueError as e:
        raise ValueError("Incorrect date format, should be YYYY-MM-DD HH:MM:SS") from e

def calculate_working_time(start_datetime, end_datetime):
    """
    Calculate the remaining working time from start_datetime to end_datetime,
    excluding weekends and holidays.
    """
    # Filter out the weekends and holidays
    working_days = [dt for dt in rrule(DAILY, dtstart=start_datetime, until=end_datetime, byweekday=(MO,TU,WE,TH,FR))
                    if dt.strftime('%Y-%m-%d') not in holidays]
    
    total_seconds = sum((min(dt + timedelta(days=1), end_datetime) - max(dt, start_datetime)).total_seconds()
                        for dt in working_days)
    
    # Convert total_seconds to days, hours, minutes, and seconds
    days, remainder = divmod(total_seconds, 86400)
    hours, remainder = divmod(remainder, 3600)
    minutes, seconds = divmod(remainder, 60)
    
    return int(days), int(hours), int(minutes), int(seconds)

if __name__ == '__main__':
    # Example usage
    start_datetime = datetime.now()
    end_datetime = parse_date('2040-12-31 23:59:59')
    remaining_time = calculate_working_time(start_datetime, end_datetime)
    print(f"Remaining working time - Days: {remaining_time[0]}, Hours: {remaining_time[1]}, Minutes: {remaining_time[2]}, Seconds: {remaining_time[3]}")

