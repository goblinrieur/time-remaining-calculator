import tkinter as tk
from tkinter import messagebox
from time_calculator import parse_date, calculate_working_time
from datetime import datetime
import threading
import time

class WorkingTimeCalculatorApp(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Working Time Calculator")
        self.geometry("400x200")

        # Input field for the deadline
        self.deadline_label = tk.Label(self, text="Enter Deadline (YYYY-MM-DD HH:MM:SS):")
        self.deadline_label.pack()

        self.deadline_entry = tk.Entry(self)
        self.deadline_entry.pack()

        # Calculate button
        self.calculate_button = tk.Button(self, text="Calculate", command=self.start_calculation_thread)
        self.calculate_button.pack()

        # Exit button
        self.exit_button = tk.Button(self, text="Exit", command=self.destroy)
        self.exit_button.pack()

        # Label to display remaining time
        self.remaining_time_label = tk.Label(self, text="Remaining Time: Days: -, Hours: -, Minutes: -, Seconds: -")
        self.remaining_time_label.pack()

    def calculate_remaining_time(self):
        """Handles the calculation and updating of the remaining time."""
        deadline_str = self.deadline_entry.get()
        try:
            end_datetime = parse_date(deadline_str)
            while True:
                now = datetime.now()
                if now >= end_datetime:
                    self.remaining_time_label.config(text="Deadline Reached!")
                    break
                days, hours, minutes, seconds = calculate_working_time(now, end_datetime)
                self.remaining_time_label.config(text=f"Remaining Time: Days: {days}, Hours: {hours}, Minutes: {minutes}, Seconds: {seconds}")
                time.sleep(1)  # Update every second
        except ValueError as e:
            messagebox.showerror("Error", str(e))

    def start_calculation_thread(self):
        """Starts a thread for the calculation to keep the GUI responsive."""
        calculation_thread = threading.Thread(target=self.calculate_remaining_time, daemon=True)
        calculation_thread.start()

if __name__ == "__main__":
    app = WorkingTimeCalculatorApp()
    app.mainloop()
